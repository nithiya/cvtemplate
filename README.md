# cvtemplate <!-- omit in toc -->

[LaTeX](https://www.latex-project.org/) template for CVs and cover letters. I made this template for my personal use. See the PDF result [here](https://gitlab.com/nithiya/cvtemplate/-/jobs/artifacts/main/file/cvmain.pdf?job=pdf) [[download](https://gitlab.com/nithiya/cvtemplate/-/jobs/artifacts/main/raw/cvmain.pdf?job=pdf)].

## Table of contents <!-- omit in toc -->

- [Requirements and compilation](#requirements-and-compilation)
- [Credits](#credits)
- [License](#license)

## Requirements and compilation

All packages used are available on [CTAN](https://www.ctan.org/). It is recommended to use a TeX distribution, such as [TeX Live](https://tug.org/texlive/), to ensure all requirements are satisfied.

The [`article`](https://www.ctan.org/pkg/article) document class is used.

The PDF file is built using either [XeLaTeX](http://xetex.sourceforge.net/) or [LuaLaTeX](http://luatex.org/) via [Arara](https://gitlab.com/islandoftex/arara):

```sh
arara cvmain.tex
```

The Arara directives used are as follows (replace `xelatex` with `lualatex`, if necessary):

```latex
% arara: xelatex
% arara: xelatex
```

Note that Arara requires [Java](https://jdk.java.net/).

An alternative is to use the latest TeX Live Docker image by [Island of TeX](https://gitlab.com/islandoftex/images/texlive), which can also be used with GitLab CI. The following is a minimal example of a valid `.gitlab-ci.yml` configuration:

```yml
image: registry.gitlab.com/islandoftex/images/texlive:latest

build:
  script:
    - arara cvmain.tex
  artifacts:
    paths:
      - ./*.pdf
```

The resulting PDF file will be available as an artifact once the build is complete. See this [TUGboat article](https://tug.org/TUGboat/tb40-3/tb126island-docker.pdf) for more information.

## Credits

This template is based on [`moderncv` – a modern curriculum vitae class](https://www.ctan.org/pkg/moderncv) and [Deedy-Resume](https://github.com/deedy/Deedy-Resume).

Examples may be derived from [TeX Stack Exchange](https://tex.stackexchange.com) (CC-BY-SA), [Wikibooks](https://en.wikibooks.org/wiki/LaTeX) (CC-BY-SA), CTAN documentation, [Dickimaw Books](https://www.dickimaw-books.com/latexresources.html), and [Overleaf](https://www.overleaf.com/learn).

## License

This work is licensed under the terms of the [LaTeX Project Public License, Version 1.3c (LPPL-1.3c)](https://www.latex-project.org/lppl/).
