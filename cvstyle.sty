%% cvstyle.sty
%% Repository: https://gitlab.com/nithiya/cvtemplate
%% Copyright 2021 N. M. Streethran (nmstreethran at gmail dot com)
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is N. M. Streethran.
%
% This work consists of the following files: cvstyle.sty and
% cvmain.tex.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% to avoid overfull margins
\emergencystretch3em

% set date format
\usepackage[british]{datetime2}

% to remove indentation and add spaces between paragraphs
\usepackage{parskip}
% line spacing
\usepackage{setspace}

% fonts, icons, and colours
\usepackage{fontawesome5}
\usepackage[rm]{roboto}
\usepackage[default]{lato}
\usepackage{pifont}
\usepackage[svgnames]{xcolor}

% remove heading number
\setcounter{secnumdepth}{0}
% modify title font and style
\usepackage{titlesec}
\titleformat{\section}{\color{\accent}\robotoslab\Large\bfseries}{
  \thesection}{1em}{}[{\titlerule[.8pt]}]

% to modify bullet points and lists
\usepackage{enumitem}
\setlist{itemsep=2.25pt,label={\color{\accent}\Pifont{pzd}{\char228}}}
% custom list for author links
\newlist{links}{itemize*}{1}
\setlist[links]{itemjoin={},label={~~},afterlabel={}}

% to use multiple columns
\usepackage{paracol}

% enable hyperlinks and hide coloured boxes around them
\usepackage[hidelinks]{hyperref}
% PDF metadata
\hypersetup{pdftitle={\name's CV},pdfauthor={\name}}

% set the document's paper and margin sizes
\usepackage[a4paper]{geometry}
\geometry{lmargin=1.5cm,rmargin=1.5cm,tmargin=1.5cm,bmargin=1.5cm}

% modify page numbers
\usepackage{fancyhdr}
\usepackage{zref-totpages}
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
% page numbers are disabled if the document is one page long
\fancyfoot[R]{
  \ifnum\ztotpages=1
  \else
  \raisebox{\height}{\color{\logos}\thepage/\ztotpages}
  \fi
}

% defining personal details
% add new links as follows:
% \newcommand{<COMMAND>}{<icon>~\href{<link>}{<command>}}
% <command> defines the text that will appear in the document and
% should be specified in the preamble of the .tex file
% e.g., \def <command> {text to display}
% <COMMAND> is used in makecvtitle (see below)
% see the fontawesome5 package documentation
% for the full list of available icons
\newcommand{\Name}{{\color{\accent}\robotoslab\huge\bfseries\name}}
\newcommand{\Phone}{\faMobile*~~\phone~~}
\newcommand{\Email}{\faEnvelope~~\href{mailto:\email}{\email}~~}
\ifdefined\website
\newcommand{\Website}{\faLink~~\href{https://\website}{\website}~~}
\fi
\ifdefined\linkedin
\newcommand{\LinkedIn}
  {\faLinkedin~~\href{https://www.linkedin.com/in/\linkedin}{\linkedin}~~}
\fi
\ifdefined\twitter
\newcommand{\Twitter}
  {\faTwitter~~\href{https://twitter.com/\twitter}{\twitter}~~}
\fi
\ifdefined\github
\newcommand{\GitHub}{\faGithub~~\href{https://github.com/\github}{\github}~~}
\fi
\ifdefined\gitlab
\newcommand{\GitLab}{\faGitlab~~\href{https://gitlab.com/\gitlab}{\gitlab}~~}
\fi
\ifdefined\orcid
\newcommand{\ORCID}{\faOrcid~~\href{https://orcid.org/\orcid}{\orcid}~~}
\fi

% defining the title elements
% links are specified conditionally within the links environment
% \ifdefined <command>
% \item <COMMAND>
% \fi
\newcommand{\makecvtitle}{
  \begin{center}
    \Name \\[8pt]
    \textcolor{\logos}{\address \\
      \begin{minipage}{.72\textwidth}
        \centering
        \begin{links}
          \centering
          \item \Phone
          \item \Email
          \ifdefined\website
          \item \Website
          \fi
          \ifdefined\linkedin
          \item \LinkedIn
          \fi
          \ifdefined\twitter
          \item \Twitter
          \fi
          \ifdefined\github
          \item \GitHub
          \fi
          \ifdefined\gitlab
          \item \GitLab
          \fi
          \ifdefined\orcid
          \item \ORCID
          \fi
        \end{links}
      \end{minipage}
    }
  \end{center}
}

% defining macros for the various CV sections
\newcommand{\experience}[4]{\textbf{#1}\hfill#2 \\ \textit{#3}\hfill#4}

\newcommand{\education}[2]{\textbf{#1}\hfill#2}

\newcommand{\referee}[6]{
  \textbf{#1} \\ \textit{#2} \\ #3 \\ #4 \\
  \textcolor{\logos}{\faPhone}~~#5 \\
  \textcolor{\logos}{\faEnvelope}~~\href{mailto:#6}{#6}
}

\newcommand{\skill}[2]{\textbf{#1} #2}

% defining macros for the various cover letter sections
% start of the letter - addresses, salutation, and subject
\newcommand{\letterstart}{
  \makecvtitle
  \vspace{24pt}
  \begin{minipage}[t]{.7\linewidth}
    \begin{flushleft}
      \textbf{\recipient} \\
      \recipientaddress
    \end{flushleft}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{.29\linewidth}
    \begin{flushright}
      \makeatletter
      \@date
      \makeatother
    \end{flushright}
  \end{minipage}

  \vspace{8pt}
  \salutation

  \vspace{8pt}
  \textbf{\subject}
  \vspace{8pt}
}

% end of letter - signature
\newcommand{\signature}{
  \vspace{8pt}
  Yours sincerely,

  \textbf{\textit{\name}}
}

% optional command for inserting list of enclosed documents
\newcommand{\enclosure}[2]
  {{\par\vspace{8pt}\color{\logos}\textit{#1: #2}}}
